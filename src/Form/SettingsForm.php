<?php

namespace Drupal\bacon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure bacon settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bacon_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bacon.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['transliteration__machine_name__blocks'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at blocks.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__blocks'),
    ];

    $form['transliteration__machine_name__fields'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at fields.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__fields'),
    ];

    $form['transliteration__machine_name__node_types'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at node types.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__node_types'),
    ];

    $form['transliteration__machine_name__media_types'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at media types.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__media_types'),
    ];

    $form['transliteration__machine_name__paragraphs_types'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at paragraphs types.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__paragraphs_types'),
    ];

    $form['transliteration__machine_name__views'] = [
      '#type' => 'checkbox',
      '#title' => 'Turns off transliteration of machine_name at views.',
      '#default_value' => $this->config('bacon.settings')
        ->get('transliteration__machine_name__views'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bacon.settings')
      ->set('transliteration__machine_name__fields', $form_state->getValue('transliteration__machine_name__fields'))
      ->set('transliteration__machine_name__node_types', $form_state->getValue('transliteration__machine_name__node_types'))
      ->set('transliteration__machine_name__media_types', $form_state->getValue('transliteration__machine_name__media_types'))
      ->set('transliteration__machine_name__blocks', $form_state->getValue('transliteration__machine_name__blocks'))
      ->set('transliteration__machine_name__paragraphs_types', $form_state->getValue('transliteration__machine_name__paragraphs_types'))
      ->set('transliteration__machine_name__views', $form_state->getValue('transliteration__machine_name__views'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
