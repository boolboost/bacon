(function ($) {
  'use strict';

  document.addEventListener('DOMContentLoaded', function () {
    let body = document.body;
    let paddingTop = getComputedStyle(body).getPropertyValue('padding-top');

    body.style.setProperty('--toolbar-top', paddingTop);

    $(document).once('bacon-toolbar').on("drupalViewportOffsetChange.toolbar", function (event, offsets) {
      body.style.setProperty('--toolbar-top', offsets.top + 'px');
    });
  });

})(jQuery);
